import express, {Request,Response, NextFunction } from "express";
import log from "@ajar/marker";
import cron from "node-cron";
import fetch from "node-fetch";
import {exec, spawn} from "child_process";
import fs from "fs";

class App {
    app: any;
    router: any;

    constructor() {
      this.app = express();
      this.router = express.Router();
      this.router.use(express.json());
      this.app.use("/", this.router);

      this.router.post("/",(req:Request,res:Response, next:NextFunction)=>{
        const mySqlFile = fs.createWriteStream(req.headers.file_name as string, { flags: "a" });
        req.pipe(mySqlFile);
      });
      this.startServer();
    }
    
    async startServer() {
      const { PORT=3000 ,HOST="localhost" } = process.env;
      await this.app.listen(PORT,HOST);
      log.magenta("api is live on",` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
    }

     
  }
  
  const myServerInstance = new App();




// let mySqlFile;
// let client = new net.Socket();

// client.connect(1337, '127.0.0.1', function() {
// 	console.log('Connected');
// 	client.write(`${'Hello, server! Love, Client.'}`);
// });

// client.on('data', function(data) {
//     try{
//         data = JSON.parse(data.toString());
//         if(data.hasOwnProperty('file_name')){
//             mySqlFile = fs.createWriteStream(data.file_name, { flags: 'a' });
//             mySqlFile.write(data.toString());
//         }
//         else{
//             console.log(data.message);
//         }

//     }
//     catch(err){
//             mySqlFile.write(data.toString());
        
        
//     }
// });
// client.on('end',()=>{
//     mySqlFile.close();;

// })
// client.on('close', function() {
// 	console.log('Connection closed');
// });