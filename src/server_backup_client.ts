/* eslint-disable @typescript-eslint/no-explicit-any */
// require('dotenv').config();
import express from "express";
import log from "@ajar/marker";
import cron from "node-cron";
import fetch from "node-fetch";
import {exec, spawn} from "child_process";
import { ReadableStream } from "stream/web";


class App {
  app: any;
  
  constructor() {
    this.app = express();
    cron.schedule("* * * * * * ", async () => {
        const child = exec("docker exec mysql-db mysqldump -u root --password=qwerty world");
        const timestamp = Date.now();
        let date = new Date(timestamp).toString();
        date = (date.replace(/[.,/#!$%^&*;:{}=\-_`~()]/g,"-"));
        const my_file_name = date+".dump.sql";
        fetch("http://localhost:3000",{method:"POST",body: child.stdout, headers:{file_name:my_file_name}});
    } );
    this.startServer();
  }


  async startServer() {
    const { PORT=8090 ,HOST="localhost" } = process.env;
    await this.app.listen(PORT,HOST);
    log.magenta("api is live on",` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
  }
}

const myServerInstance = new App();


